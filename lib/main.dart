
import 'package:flutter/material.dart';
import 'package:flutter_puzzle_challenge/Puzzle.dart';
import 'package:flutter_puzzle_challenge/Tile.dart';
import 'package:state_groups/state_groups.dart';

Puzzle puzzle = Puzzle();
const double PADDING_SIZE = 10;
const BorderRadiusGeometry border = BorderRadius.all(Radius.circular(20));
const Color blue = Color(0xff0468d7);
StateGroup<void> mainGroup = StateGroup<void>();

void main() {
	runApp(const MyApp());
}

class MyApp extends StatelessWidget {
	const MyApp({Key? key}) : super(key: key);

	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			theme: ThemeData(primarySwatch: Colors.blue),
			//darkTheme: ThemeData.dark(),
			home: const MyHomePage(),
		);
	}
}

class MyHomePage extends StatefulWidget {
	const MyHomePage({Key? key}) : super(key: key);

	@override
	State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends SyncState<void, MyHomePage> {

	_MyHomePageState() : super(mainGroup);

	@override
	Widget build(BuildContext context) {
		return Scaffold(
			body: MediaQuery.of(context).size.width < 1300
					? buildCompact(0)
					: buildWideScreen(0),
		);
	}

	Widget buildCompact(int numPiecesInWrongPlace) {
		const double COMPACT_GRID_SIZE = 500;

		return Column(
			children: <Widget>[
				Flexible(
					flex: 1,
					child: Column(
						mainAxisAlignment: MainAxisAlignment.center,
						children: <Widget>[
							Text(numPiecesInWrongPlace == 0 ? "Well Done. Congrats!" : "Puzzle Challenge", style: const TextStyle(fontSize: 40, fontWeight: FontWeight.bold),),
							const SizedBox(height: 20),
							Text.rich(TextSpan(
								style: const TextStyle(fontSize: 20, color: blue),
								children: <InlineSpan>[
									TextSpan(text: puzzle.numMoves.toString(), style: const TextStyle(fontWeight: FontWeight.bold)),
									const TextSpan(text: " Moves | "),
									TextSpan(text: numPiecesInWrongPlace.toString(), style: const TextStyle(fontWeight: FontWeight.bold)),
									const TextSpan(text: " Tiles"),
								]
							))
						],
					),
				),
				Center(
					child: makeGrid(COMPACT_GRID_SIZE),
				),
				Flexible(
					flex: 1,
					child: Center(
						child: GestureDetector(
							child: Container(
								width: COMPACT_GRID_SIZE * (1.5 / 4),
								decoration: BoxDecoration(
									borderRadius: BorderRadius.circular(COMPACT_GRID_SIZE),
									color: blue,
								),
								height: 50,
								child: Center(
									child: Row(
										mainAxisAlignment: MainAxisAlignment.center,
										children: const <Widget>[
											Icon(Icons.refresh, size: 30, color: Colors.white,),
											SizedBox(width: 5),
											Text("Shuffle", style: TextStyle(color: Colors.white, fontSize: 25))
										],
									),
								),
							),
							onTap: () {
								puzzle.resetAndShuffle();
								setState(() {});
							},
						)
					),
				),
			],
		);
	}

	Widget buildWideScreen(int numPiecesInWrongPlace) {
		const double WIDE_GRID_SIZE = 600;

		return Stack(
			children: <Widget>[
				Padding(
					padding: const EdgeInsets.only(left: 40),
					child: Column(
						mainAxisAlignment: MainAxisAlignment.center,
						crossAxisAlignment: CrossAxisAlignment.start,
						children: <Widget>[
							const Text("Simple", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
							const SizedBox(height: 20),
							Text(numPiecesInWrongPlace == 0 ? "Well Done.\nCongrats!" : "Puzzle\nChallenge", style: const TextStyle(fontSize: 60, fontWeight: FontWeight.bold),),
							const SizedBox(height: 20),
							Text.rich(TextSpan(
								style: const TextStyle(fontSize: 20, color: blue),
								children: <InlineSpan>[
									TextSpan(text: puzzle.numMoves.toString(), style: const TextStyle(fontWeight: FontWeight.bold)),
									const TextSpan(text: " Moves | "),
									TextSpan(text: numPiecesInWrongPlace.toString(), style: const TextStyle(fontWeight: FontWeight.bold)),
									const TextSpan(text: " Tiles"),
								]
							)),
							const SizedBox(height: 30),
							GestureDetector(
								child: Container(
									width: WIDE_GRID_SIZE * (1.5 / 4),
									decoration: BoxDecoration(
										borderRadius: BorderRadius.circular(WIDE_GRID_SIZE),
										color: blue,
									),
									height: 50,
									child: Center(
										child: Row(
											mainAxisAlignment: MainAxisAlignment.center,
											children: const <Widget>[
												Icon(Icons.refresh, size: 30, color: Colors.white,),
												SizedBox(width: 5),
												Text("Shuffle", style: TextStyle(color: Colors.white, fontSize: 25))
											],
										),
									),
								),
								onTap: () {
									puzzle.resetAndShuffle();
									setState(() {});
								},
							)
						],
					),
				),
				Center(
					child: makeGrid(WIDE_GRID_SIZE),
				),
			],
		);
	}

	Widget makeGrid(double size) {
		final List<Widget> cells = <Widget>[];

		for (int i = 0; i < puzzle.puzzlePieces.length; i++) {
			if (puzzle.puzzlePieces[i] == null) {
				cells.add(Container());
			} else {
				cells.add(Tile.fromIndices(puzzle.puzzlePieces[i]!, (size * 0.25) - PADDING_SIZE, i % 4, i ~/ 4, key: Key(puzzle.puzzlePieces[i].toString())));
			}
		}

		return Column(
			children: <Widget>[
				SizedBox(
					width: size,
					height: size * 0.25,
					child: Center(
						child: Text(puzzle.topRequirementsString),
					),
				),
				SizedBox(
					height: size,
					width: size,
					child: Stack(
						children: cells,
					),
				),
				SizedBox(
					width: size,
					height: size * 0.25,
					child: Center(
						child: Text(puzzle.bottomRequirementsString),
					),
				),
			],
		);
	}
}
