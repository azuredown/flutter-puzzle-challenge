
import 'dart:math';

import 'package:tools/RandomBag.dart';
import 'package:tuple/tuple.dart';

enum PossibleSwap {
	LEFT, UP, DOWN, RIGHT,
}

final Random rng = Random();

bool canSwapHoleWithLeft(int holeIndex) => holeIndex % 4 != 0;
bool canSwapHoleWithRight(int holeIndex) => holeIndex % 4 != 3;
bool canSwapHoleWithUp(int holeIndex) => holeIndex >= 4;
bool canSwapHoleWithDown(int holeIndex) => holeIndex <= 11;

void swapHoleWithLeft(int holeIndex, List<int?> puzzle) => swap(holeIndex, holeIndex - 1, puzzle);
void swapHoleWithRight(int holeIndex, List<int?> puzzle) => swap(holeIndex, holeIndex + 1, puzzle);
void swapHoleWithUp(int holeIndex, List<int?> puzzle) => swap(holeIndex, holeIndex - 4, puzzle);
void swapHoleWithDown(int holeIndex, List<int?> puzzle) => swap(holeIndex, holeIndex + 4, puzzle);

Tuple4<int, int, int, int> getTopRequirements(List<int?> futurePuzzle) {
	return Tuple4<int, int, int, int>(
		futurePuzzle[0] ?? futurePuzzle[4]!,
		futurePuzzle[1] ?? futurePuzzle[5]!,
		futurePuzzle[2] ?? futurePuzzle[6]!,
		futurePuzzle[3] ?? futurePuzzle[7]!,
	);
}

Tuple4<int, int, int, int> getBottomRequirements(List<int?> futurePuzzle) {
	return Tuple4<int, int, int, int>(
		futurePuzzle[12] ?? futurePuzzle[8]!,
		futurePuzzle[13] ?? futurePuzzle[9]!,
		futurePuzzle[14] ?? futurePuzzle[10]!,
		futurePuzzle[15] ?? futurePuzzle[11]!,
	);
}

void swap(int piece1, int piece2, List<int?> puzzle) {
	final int? temp = puzzle[piece1];
	puzzle[piece1] = puzzle[piece2];
	puzzle[piece2] = temp;
}

List<int?> simulateRandomSwaps(List<int?> tilesToCopy, int numSwaps, {bool Function(List<int?>)? keepGoing}) {
	final List<int?> ourTiles = tilesToCopy.toList();
	final List<PossibleSwap> swaps = <PossibleSwap>[];
	int counter = 1;
	while (true) {
		swaps.clear();
		final int holeIndex = ourTiles.indexOf(null);
		if (canSwapHoleWithLeft(holeIndex)) {
			swaps.add(PossibleSwap.LEFT);
		}
		if (canSwapHoleWithRight(holeIndex)) {
			swaps.add(PossibleSwap.RIGHT);
		}
		if (canSwapHoleWithUp(holeIndex)) {
			swaps.add(PossibleSwap.UP);
		}
		if (canSwapHoleWithDown(holeIndex)) {
			swaps.add(PossibleSwap.DOWN);
		}
		final PossibleSwap chosenSwap = swaps[rng.nextInt(swaps.length)];
		if (chosenSwap == PossibleSwap.LEFT) {
			swapHoleWithLeft(holeIndex, ourTiles);
		} else if (chosenSwap == PossibleSwap.RIGHT) {
			swapHoleWithRight(holeIndex, ourTiles);
		} else if (chosenSwap == PossibleSwap.UP) {
			swapHoleWithUp(holeIndex, ourTiles);
		} else if (chosenSwap == PossibleSwap.DOWN) {
			swapHoleWithDown(holeIndex, ourTiles);
		}
		counter++;
		if (counter >= numSwaps && (keepGoing == null || !keepGoing(ourTiles))) {
			break;
		}
	}
	return ourTiles;
}

RandomBag<int> getRandomBagOfUnusedNumbers(List<int?> usedNumbers) {
	final Set<int?> numbers = usedNumbers.toSet();
	final RandomBag<int> returnBag = RandomBag<int>();
	for (int i = 1; i <= 99; i++) {
		if (!numbers.contains(i)) {
			returnBag.add(i);
		}
	}
	return returnBag;
}

extension Contains on Tuple4<int, int, int, int> {
	bool contains(int item) {
		return item1 == item || item2 == item || item3 == item || item4 == item;
	}

	bool containsAny(Tuple4<int, int, int, int> item) {
		return contains(item.item1) || contains(item.item2) || contains(item.item3) || contains(item.item4);
	}
}
