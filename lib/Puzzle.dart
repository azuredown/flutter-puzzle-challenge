

import 'package:flutter_puzzle_challenge/PuzzleFunctions.dart';
import 'package:flutter_puzzle_challenge/main.dart';
import 'package:tools/RandomBag.dart';
import 'package:tuple/tuple.dart';

class Puzzle {

	Puzzle() {
		resetAndShuffle();
	}

	int? highlightedValue;
	List<int?> puzzlePieces = <int?>[];
	int numMoves = 0;
	late Tuple4<int, int, int, int> topRequirements, bottomRequirements;

	// Resets everything and shuffles the tiles with the following algorithm:
	// start sorted and then apply 1000 random shuffles
	void resetAndShuffle() {
		numMoves = 0;
		highlightedValue = null;

		final RandomBag<int> bag = RandomBag<int>();

		for (int i = 1; i <= 99; i++) {
			bag.add(i);
		}

		puzzlePieces.clear();

		for (int i = 0; i < 16; i++) {
			puzzlePieces.add(bag.getRandomItem(rng));
		}

		puzzlePieces[rng.nextInt(puzzlePieces.length)] = null;

		// Get top and bottom requirements
		final List<int?> afterRandomSwaps = simulateRandomSwaps(puzzlePieces, 1000);
		topRequirements = getTopRequirements(afterRandomSwaps);
		bottomRequirements = getBottomRequirements(afterRandomSwaps);
	}

	String getRequirements(Tuple4<int, int, int, int> requirements) {
		return "${requirements.item1.toString().padLeft(2, "0")} ${requirements.item2.toString().padLeft(2, "0")} ${requirements.item3.toString().padLeft(2, "0")} ${requirements.item4.toString().padLeft(2, "0")}";
	}

	String get topRequirementsString => getRequirements(topRequirements);
	String get bottomRequirementsString => getRequirements(bottomRequirements);

	void trySwapWholeWith(int num) {
		final int numIndex = puzzlePieces.indexOf(num);
		final int holeIndex = puzzlePieces.indexOf(null);

		void onSuccess() {

			if (puzzlePieces[0] == topRequirements.item1 && puzzlePieces[1] == topRequirements.item2 &&
					puzzlePieces[2] == topRequirements.item3 && puzzlePieces[3] == topRequirements.item4) {
				puzzlePieces[0] = puzzlePieces[1] = puzzlePieces[2] = puzzlePieces[3] = null;
				final RandomBag<int> unusedNumbers = getRandomBagOfUnusedNumbers(puzzlePieces);
				puzzlePieces[0] = unusedNumbers.getRandomItem(rng);
				puzzlePieces[1] = unusedNumbers.getRandomItem(rng);
				puzzlePieces[2] = unusedNumbers.getRandomItem(rng);
				puzzlePieces[3] = unusedNumbers.getRandomItem(rng);
				final List<int?> afterRandomSwaps = simulateRandomSwaps(puzzlePieces, 500, keepGoing: (List<int?> tiles) {
					return bottomRequirements.containsAny(getTopRequirements(tiles));
				});
				topRequirements = getTopRequirements(afterRandomSwaps);
			}
			if (puzzlePieces[12] == bottomRequirements.item1 && puzzlePieces[13] == bottomRequirements.item2 &&
					puzzlePieces[14] == bottomRequirements.item3 && puzzlePieces[15] == bottomRequirements.item4) {
				puzzlePieces[15] = puzzlePieces[14] = puzzlePieces[13] = puzzlePieces[12] = null;
				final RandomBag<int> unusedNumbers = getRandomBagOfUnusedNumbers(puzzlePieces);
				puzzlePieces[15] = unusedNumbers.getRandomItem(rng);
				puzzlePieces[14] = unusedNumbers.getRandomItem(rng);
				puzzlePieces[13] = unusedNumbers.getRandomItem(rng);
				puzzlePieces[12] = unusedNumbers.getRandomItem(rng);
				final List<int?> afterRandomSwaps = simulateRandomSwaps(puzzlePieces, 500, keepGoing: (List<int?> tiles) {
					return topRequirements.containsAny(getBottomRequirements(tiles));
				});
				bottomRequirements = getBottomRequirements(afterRandomSwaps);
			}
			highlightedValue = num;
			numMoves++;
			mainGroup.notifyAll();
		}

		if (numIndex == holeIndex - 1 && canSwapHoleWithLeft(holeIndex)) {
			swapHoleWithLeft(holeIndex, puzzlePieces);
			onSuccess();
		} else if (numIndex == holeIndex + 1 && canSwapHoleWithRight(holeIndex)) {
			swapHoleWithRight(holeIndex, puzzlePieces);
			onSuccess();
		} else if (numIndex == holeIndex - 4 && canSwapHoleWithUp(holeIndex)) {
			swapHoleWithUp(holeIndex, puzzlePieces);
			onSuccess();
		} else if (numIndex == holeIndex + 4 && canSwapHoleWithDown(holeIndex)) {
			swapHoleWithDown(holeIndex, puzzlePieces);
			onSuccess();
		}
	}

}
