
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_puzzle_challenge/main.dart';

class Tile extends StatefulWidget {
	const Tile(this.num, this.size, this.offset, {Key? key}) : super(key: key);

	factory Tile.fromIndices(int num, double size, int x, int y, {Key? key}) {
		return Tile(
			num, size,
			Offset(x * (size + PADDING_SIZE) + PADDING_SIZE * 0.5, y * (size + PADDING_SIZE) + PADDING_SIZE * 0.5),
			key: key,
		);
	}

	final int num;
	final double size;
	final Offset offset;

	@override
	TileState createState() => TileState();
}

class TileState extends State<Tile> with SingleTickerProviderStateMixin {

	TileState();

	Offset? oldOffset;

	late AnimationController _controller;
	late Animation<double> _animation;

	@override
	void didUpdateWidget(Tile oldWidget) {

		oldOffset = oldWidget.offset;
		_controller.forward(from: 0);

		super.didUpdateWidget(oldWidget);
	}

	@override
	void initState() {
		_controller = AnimationController(vsync: this, duration: const Duration(milliseconds: 200));
		_controller.value = 0;
		_animation = Tween<double>(begin: 0, end: 1).animate(CurvedAnimation(parent: _controller, curve: Curves.easeOutQuart));
		super.initState();
	}

	@override
	void dispose() {
		_controller.dispose();
		super.dispose();
	}

	@override
	Widget build(BuildContext context) {
		return AnimatedBuilder(
			animation: _controller,
			builder: (_, __) {
				return Transform.translate(
					offset: oldOffset == null ? widget.offset : Offset.lerp(oldOffset, widget.offset, _animation.value)!,
					child: SizedBox(
						height: widget.size,
						width: widget.size,
						child: Material(
							color: widget.num == puzzle.highlightedValue ? const Color(0xff13b9fd) : blue,
							borderRadius: border,
							child: InkWell(
								customBorder: const RoundedRectangleBorder(borderRadius: border),
								hoverColor: Colors.black12,
								child: SizedBox(
									height: widget.size,
									width: widget.size,
									child: Center(
										child: Text(
											widget.num.toString().padLeft(2, '0'),
											style: const TextStyle(
												color: Colors.white,
												fontSize: 50,
												fontWeight: FontWeight.w900
											),
										),
									),
								),
								onTap: () {
									puzzle.trySwapWholeWith(widget.num);
								},
							),
						),
					),
				);
			}
		);
	}
}
